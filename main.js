const modes = {
	Bars: [0x304, 0x305],
	Ticks: [0x300, 0x301],
	Dots: [0x308, 0x307]
}


var vm = new Vue({
    el: '#app',
    data: {
        text: "Hello",
		height: "5",
		mode: "Bars"
    },
    computed: {
    },
	methods: {
		example: function(mode) {
			return addDiacritics("Like This", mode, 2)
		},
        malformed: function() {
			if(!vm) {
				console.log('no app')
				return ""
			}
			let height = this.height
			if(height == "") {
				height = 5
			} else if(!(typeof parseInt(height) == "number" && parseInt(height) > 0)) {
				console.log('height isn\'t a number')
				return ""
			} else {
				height = parseInt(this.height)
			}

			console.log('diacritics added')
            return addDiacritics(this.text, this.mode, height)
        }
	}
})
vm.$forceUpdate();


function addDiacritics(text, mode, height) {


    let diacritics = modes[mode]
    let out = []

    for (let i in text) {
        out.push(text.charCodeAt(i))
        for (let j = 0; j < height; j++)
            out.push(diacritics[j % diacritics.length])
    }

    return String.fromCharCode.apply(null, out)
}
