let numDiacritics = 100


//0x1AB0



let diacritics = [
	0x304,
	0x305//,
	// 0x303,
	// 0x304,
	// 0x305,
	// 0x306,
	// 0x307,
	// 0x308,
	// 0x30a,
	// 0x30b,
	// 0x30c,
	// 0x30f,
	// 0x311,
	// 0x313,
]

// var diacritics = [/*"̀", "́", "̂", "̃", "̄", "̅", "̆", "̇", "̈", "̉", "̊", "̋", "̌", "̍", "̎", "̏", "̐", "̑", "̒", "̓", "̔", "̕", "̚", "̛", "̽", "̾", "̿", "̀", "́", "͂", "̓", "̈́", "͆", "͊", "͋", "͌", "͐", "͑", "͒", "͗", "͘", "͛", "͝", "͝", "͠",*/ "͡"].map(a => a.charCodeAt(0))


function choose(list) {
	return list[Math.floor(Math.random()*list.length)]
}

let argv = process.argv
argv.shift()
argv.shift()
let arg = argv.join(' ')

let out = []

for(let i in arg) {
	out.push(arg.charCodeAt(i))
	for(let j = 0; j < numDiacritics; j++)
		out.push(diacritics[j % diacritics.length])
}


console.log(String.fromCharCode.apply(null, out))
